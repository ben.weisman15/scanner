- The application's entry point is server.js
- the application assume the exiestence of 3 repos - repoA,repoB,repoC, in a user's github account. 
- user: ben-weisman
- 
- 2 scenarios were defined:
    - getReposList
    - getRepoDetails(name: <REPO_NAME>)

- scenarios query structure:

-------------------
{
  getReposList(){
    name
    owner
    size
  }
}

-------------------
{
 	getRepoDetails(name:"repo name"){
    details{
      owner
      name
      size
    }
    visibility
    fileContent {
      name
      path
      content
    }
    hooks {
      name
      active
      type
    }
    numFiles
    
  }
}

-------------------