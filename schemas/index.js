
const graphql = require('graphql');
const {GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList} = graphql;
const requestHandler = require('./../services/requestHandler')
const {RepoType,RepoDetailsType} = require('./typedef/repoTypes')


const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
       getReposList : {
        type: new GraphQLList(RepoType),
        args: {},
        resolve (parent,args) {
            return new Promise((resolve,reject) => {
                requestHandler.showRepos()
                .then((data) => {
                    return resolve(data);
                });
            })
        }
       }
       ,
       getRepoDetails : {
        type: RepoDetailsType,
        args: {name: {type: GraphQLString}},
        resolve (parent,args) {
            
            return new Promise((resolve,reject) => {
                requestHandler.repoDetails(args.name)
                .then((data) => {
                    return resolve(data);
                })
                .catch((err) => {
                    console.log(err);
                    reject(err);
                });
            })
            .catch((err) => {
                console.log(err);
            })

        }
       }
    }
});

module.exports = new graphql.GraphQLSchema({query: RootQuery, mutation: null});