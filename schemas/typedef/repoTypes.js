const graphql = require('graphql');
const {GraphQLObjectType, GraphQLInt, GraphQLString,GraphQLList} = graphql;

const RepoType = new GraphQLObjectType({
    name: "Repo",
    fields: () => ({
        name: {type: GraphQLString},
        owner: {type: GraphQLString},
        size: {type: GraphQLInt}
    })
});

const RepoDetailsType = new GraphQLObjectType({
    name: "RepoDetailes",
    fields: () => ({
        details: {type: RepoType},
        visibility: {type: GraphQLString},
        hooks: {type: new GraphQLList(HooksType)},
        fileContent: {type: FileContentType},
        numFiles: {type: GraphQLInt}
    })
});


const FileContentType = new GraphQLObjectType({
    name: "FileContentType",
    fields: () => ({
        name: {type: GraphQLString},
        path: {type: GraphQLString},
        content: {type: GraphQLString}
    })
});



const HooksType = new GraphQLObjectType ({
    name: "Hooks",
    fields: () => ({
        name: {type: GraphQLString},
        active: {type: graphql.GraphQLBoolean},
        type: {type: GraphQLString}
    })
})

module.exports = {RepoType,RepoDetailsType,HooksType};