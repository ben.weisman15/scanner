const { generateOptions } = require('../utils/utils');
const app = require('./../server');
const collector = require('./collector')
require('dotenv').config();




const populateReposList = (data) => {
    let keys = Object.keys(data);
    let res = [];
    for (var i =0;i<keys.length;i++){
        let record = {};
        record.name = data[keys[i]].name;
        record.owner = data[keys[i]].owner.login;
        record.size = data[keys[i]].size;
        res.push(record);
        }
    return res;
}



const generateDetailsTypeObject = (data) => {
    let details = {};
    details.name = data.name;
    details.owner = data.owner.login;
    details.size = data.size;
    return details;
}


const generateFileContentTypeObject = (fileContent) => {
    const buff = Buffer.from(fileContent.content,'base64');
    let readableContent = buff.toString('utf-8');
    ymlEntity = {};
    ymlEntity.name = fileContent.name;
    ymlEntity.path = fileContent.path;
    ymlEntity.content = readableContent;
    return ymlEntity;
}


const repoDetails = (repoName) => {
    return new Promise ((resolve,reject) => {
        let res = {};
        let options = generateOptions('/repos/ben-weisman/' + repoName);

        collector.fetch(options)
        .then((data) => {

            res.details = generateDetailsTypeObject(data);
            res.visibility = data.visibility;
            options = generateOptions(`/repos/Ben-Weisman/${repoName}/hooks`);
            collector.fetch(options)
            .then((hooks) => {
                console.log('\n\nhooks: ');
                console.log(hooks)
                const result = hooks.filter(hook => hook.active == true);
                res.hooks = result;

                options = generateOptions(`/search/code?q=user:ben-weisman+repo:ben-weisman/${repoName}+extension:yml`);
                collector.fetch(options)
                .then((data) => {
                    
                    if (data.items.length == 0)
                        res.ymlContent = 'No yml files were found in repo';
                    else {
                        let file = data.items[0];
                        let path = file.path;    

                        options = generateOptions(`/repos/ben-weisman/${repoName}/contents/${path}`);
                        collector.fetch(options)
                        .then((fileContent) => {

                            res.fileContent = generateFileContentTypeObject(fileContent);
                            options = generateOptions(`/repos/ben-weisman/${repoName}/git/trees/master?recursive=1`)
                            collector.fetch(options)
                            .then((data) => {
                                const numBlobs = data.tree.filter(item => item.type == 'blob');
                                res.numFiles = numBlobs.length;
                                return resolve(res);
                            })
                        })
                    }
                })
                .catch((err) => {
                    reject(err);
                })
            })
            .catch((error) => {
                reject(error);
            });
 
        })
        .catch((error) => {
            reject(error);
        });
    })
}


const showRepos = async () => {
    return new Promise ((resolve,reject) => {
        const options = generateOptions('/users/ben-weisman/repos');
        collector.fetch(options)
        .then ((data) => {
            const res = populateReposList(data);
            resolve(res);
        })
        .catch((err) => {
            reject(err);
        });
    })
}
module.exports = {showRepos,repoDetails}