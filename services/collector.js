const https = require('https');
require('dotenv').config();


const fetch = (options) => {
    return new Promise ((resolve,reject) => {
        let data = '';
        https.get(options, function(resp) {
            resp.on("data", function(chunk) {
                data += chunk;
            });
            resp.on("end", () => {
                let jsonData = JSON.parse(data);
                return resolve(jsonData);
                });
            resp.on('error', err => {
                console.log('Error: ' + err.message);
                reject(err);
            });
        });
    })
    
}

module.exports = {fetch}

