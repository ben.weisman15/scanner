const constants=require('./../constants');
require('dotenv').config();

const generateOptions=(_path)=>{
    return options = {
        hostname: constants.hostname,
        path: _path,
        headers: {
            'User-Agent': constants.user_agent,
            "Authorization": "Token " + process.env.GITHUB_ACCESS_TOKEN
        },
        OAUth: process.env.GITHUB_ACCESS_TOKEN
    }
}

module.exports ={ generateOptions }