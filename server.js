const express = require('express');
const cors = require('cors');
const {graphqlHTTP} = require('express-graphql');
const schema = require('./schemas');
const middlewares = require('./middlewares');

const PORT = 3000;
const app  = express();




app.use(express.json());
app.use(cors());
app.use(middlewares.setHeaders)
app.use('/graphql',graphqlHTTP({
    schema,
    graphiql: true
}));




app.listen((PORT), () => {
    
    console.log("Server running");
});

module.exports = app;